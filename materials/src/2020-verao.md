Oficinas de Verão - Fevereiro 2020
==================================

### 1º dia: Introdução à (In)segurança na Web

Ministrante: Gustavo Dutra

- _Materiais logo logo :)_

### 2º dia: Introdução ao LaTeX

Ministrante: Gustavo Emanuel Kundlatsch

- _Materiais logo logo :)_

### 3º dia: Introdução ao Processing e ao p5.js

Ministrante: João Vitor Maia Neves Cordeiro

- _Materiais logo logo :)_

### 4º dia: Programação Assíncrona com Python

Ministrante: Tarcísio Eduardo Moreira Crocomo

- _Materiais logo logo :)_

### 5º dia: Introdução à (In)segurança na Web

Ministrante: João Paulo TIZ

- [Material aqui assim que completo](/workshops/intro-arch)
