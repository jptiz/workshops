#!/usr/bin/env bash
CURDIR=$(pwd)

curl -O "http://${URL}:8000/devkitpro.tar.gz"
tar -xf devkitpro.tar.gz
export DEVKITPRO=${CURDIR}/devkitpro
export DEVKITARM=${DEVKITPRO}/devkitarm
export PATH="${DEVKITARM}:${PATH}"

echo "export DEVKITPRO=\"${DEVKITPRO}\"" >> "${HOME}/.bashrc"
echo "export DEVKITARM=\"${DEVKITARM}\"" >> "${HOME}/.bashrc"
echo "export PATH=\"\${DEVKITARM}:\${PATH}\"" >> "${HOME}/.bashrc"
