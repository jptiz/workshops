#include "quickgba.h"

constexpr auto face =
"                              "
"                              "
"           YYYYYYY            "
"         YYYYYYYYYYY          "
"        YYYYYYYYYYYYY         "
"       YYW  YYYYW  YYY        "
"      YYWW  YYYWW  YYYY       "
"      YYWWWWYYYWWWWYYYY       "
"     YYYYYYYYYYYYYYYYYYY      "
"     YYYYYYYYYYYYYYYYYYY      "
"     YYY            YYYY      "
"     YYYY rrrrrrrrrr YYY      "
"      YYY rrrrrrrrrr YY       "
"      YYY rrrrPPPPrr YY       "
"       YYY rrPPPPPP YY        "
"        YYY rPPPPP YY         "
"         YYYY    YYY          "
"           YYYYYYY            "
"                              "
"                              ";


auto color_from_table(char entry) {
    using gba::Color;

    switch (entry) {
        case 'Y': return Color{31, 31, 0};
        case 'R': return Color{31, 0, 0};
        case 'r': return Color{31, 15, 15};
        case 'P': return Color{31, 24, 28};
        case 'G': return Color{0, 31, 0};
        case 'B': return Color{0, 0, 31};
        case 'W': return Color{31, 31, 31};
    }

    return Color{};
}

int main() {
    using gba::Color;

    gba::setup_mode_3();

    auto width = gba::screen_width() / 8;
    auto height = gba::screen_height() / 8;

    for (auto y = 0; y < height; ++y) {
        for (auto x = 0; x < width; ++x) {
            auto color = color_from_table(face[x + y * width]);

            auto start_x = x * 8;
            auto start_y = y * 8;

            for (auto sub_x = start_x; sub_x < start_x + 8; ++sub_x) {
                for (auto sub_y = start_y; sub_y < start_y + 8; ++sub_y) {
                    gba::screen(sub_x, sub_y) = color;
                }
            }
        }
    }

    while (true) {
    }
}
