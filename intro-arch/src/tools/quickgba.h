#ifndef QUICKGBA_H
#define QUICKGBA_H

#include <array>
#include <bitset>
#include <type_traits>

namespace gba {

/*
 * Dark magic
 */

template <typename T>
inline T& at(unsigned address) {
    return *new (reinterpret_cast<void*>(address)) T{};
}

template <>
inline uint16_t& at<uint16_t>(unsigned address) {
    return *reinterpret_cast<uint16_t*>(address);
}

template <>
inline volatile const uint16_t& at<volatile const uint16_t>(unsigned address) {
    return *reinterpret_cast<volatile const uint16_t*>(address);
}

template <typename Enum>
constexpr auto value_of(Enum e)
{
    return static_cast<std::underlying_type_t<Enum>>(e);
}

/*
 * Simple structures
 */
struct Color {
    uint16_t value;

    Color() = default;

    Color(char r, char g, char b):
        value{static_cast<uint16_t>(
            ((r & 0b11111) << 0) |
            ((g & 0b11111) << 5) |
            ((b & 0b11111) << 10)
        )}
    {}

    auto r() const -> char {
        return value & 0b11111;
    }

    auto g() const -> char {
        return (value >> 5) & 0b11111;
    }

    auto b() const -> char {
        return (value >> 10) & 0b11111;
    }
};

/*
 * GBA internal stuff
 */
namespace details {
    static auto& raw_screen = *new (reinterpret_cast<void*>(0x0600'0000)) std::array<Color, 240*160>{};
}

constexpr auto screen_width() -> int {
    return 240;
}

constexpr auto screen_height() -> int {
    return 160;
}

auto screen(int x, int y) -> Color& {
    return details::raw_screen[x + y * screen_width()];
}

/**
 * Map layers.
 */
enum class Layer {
    BG0,
    BG1,
    BG2,
    BG3,
    OBJ,
};

/**
 * Display Control Register.
 */
static auto& lcd_control = at<std::bitset<16>>(0x0400'0000);;

/**
 * Number of scanline currently being processed by PPU.
 */
static volatile const auto& vcount = at<volatile const uint16_t>(0x0400'0006);

/**
 * Shows/hide layer.
 */
inline void layer_visible(Layer layer, bool visible=true) {
    lcd_control[8 + value_of(layer)] = visible;
}

auto setup_mode_3() -> void {
    lcd_control = (lcd_control.to_ulong() & ~0b111u) | 3;
    layer_visible(Layer::BG2);
}

}

#endif
