# Roteiro - Oficina de Introdução a Arquitetura de Computadores com GBA

[Propósito desta oficina](./0-pre/proposito.md)
[Preparações](./0-pre/preparacoes.md)
- [Afinal: O que é um computador?](./1-um-computador/index.md)
    - [Componentes básicos](./1-um-computador/componentes-basicos.md)
    - [Núcleo](./1-um-computador/nucleo.md)
