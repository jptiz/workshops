Propósito desta oficina
=======================

Às vezes é complicado imaginar o que acontece quando seus programas executam, e
desconhecer os mecanismos envolvidos leva a crenças em "lendas urbanas" sobre
programação. Mas mesmo pela curiosidade: como seus programas fazem uso da
memória? Como ela sequer é organizada? O que faz especificamente um processador
e o que há de diferente entre tantos modelos?

Aqui daremos um pequeno direcionamento de como essas coisas funcionam, sem
entrar em muitos detalhes (pois eles são **complexos!**), mas ao menos tocando
em detalhes que são divertidos (pra mim).

E, é claro, utilizaremos um GBA porque é um console antigo, muito bem
documentado, fácil de fazer homebrew e...vamos lá, é um GBA. Quem que conhecia
GBA e não queria fazer seu próprio Pokémon?
