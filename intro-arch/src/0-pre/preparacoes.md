Preparações
===========

Antes de mais nada, vamos nos certificar de que está tudo correto.

> **IMPORTANTE**: Nos comandos, o `$` no **início** dele **não faz parte do
> comando**, é apenas para separar:
>
> ```console
> $ isto-é-um-comando
> Isto não é um comando, é o que só o resultado do "isto-é-um-comando"
> $ isto-é-outro-comando
> ```

Baixando as ferramentas
-----------------------

Precisaremos de:
- DevkitARM (_toolchain_ para ARM);
- MGBA-Qt (emulador de GBA, já presente na ISO do PET).

Se estiver em um Arch/Manjaro/etc. com permissões de administrador, instale
`devkitarm` pelo AUR. Se estiver nas máquinas do laboratório do INE:

1. Veja qual o IP que está no quadro;
2. Baixe [este script de instalação da
   devkitpro](./src/tools/download-and-install-devkitpro.sh);
2. Abra um terminal, vá na pasta em que o script foi baixado, e execute:

   ```console
   $ URL=IP-que-está-no-quadro ./download-and-install-devkitpro.sh
   ```

4. Ainda no terminal, verifique se é exibida a versão do compilador:

   ```console
   $ arm-none-eabi-g++ --version
   arm-none-eabi-g++ (devkitARM release 53) 9.1.0
   <coisas>
   ```

Testando se as ferramentas funcionam
------------------------------------

1. Abra um terminal;
2. Crie uma pasta para esta oficina (e entre nela), por exemplo:

   ```console
   $ mkdir intro-arq-gba
   $ cd intro-arq-gba
   ```

3. Baixe e extraia as ferramentas + exemplo simples:

   ```console
   $ curl -O "https://jptiz.gitlab.io/workshops/intro-arch/tools.tar.gz"
   $ tar -xf tools.tar.gz
   $ cd tools
   ```

4. Execute o comando abaixo para preparar uma parte das ferramentas:

   ```console
   $ make -C specs
   ```

5. Compile o exemplo usando a nossa ferramenta:

  ```console
  $ arm-none-eabi-g++ awesome.cpp -fno-exceptions -fno-rtti -o awesome.gba -specs=specs/gba.specs
  ```
