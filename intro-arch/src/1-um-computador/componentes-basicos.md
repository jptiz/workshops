Componentes básicos
===================

De uma maneira ligeiramente mais técnica que a vista antes, quando falamos de
"o que é um computador", estamos falando disso:

```mermaid
graph LR
    N[Núcleo]

    subgraph Processador
        N
    end
```

O **Núcleo** é quem executa efetivamente as instruções (e será melhor explicado
na seção seguinte). Mas esse diagrama não é muito explicativo: quais instruções
ele vai executar? De onde elas vêm?  Para isso, podemos criar uma grande
caixinha que "lembra" (guarda) informações - ou seja, uma **memória**-, e a
informação guardada nela será especificamente quais as instruções do programa.

```mermaid
graph LR
    MI[Instruções]
    N[Núcleo]

    subgraph Memória
        MI
    end

    subgraph Processador
        N
    end

    MI-->N
```

A Memória de Instruções é, em essência, o programa em si. Qualquer coisa além
disso é meramente consequência de executar esse programa - ou seja, de seguir o
que o as instruções mandam.

Nas próximas seções, iremos desvendar como funcionam esses pequenos
componentes.

Visualizando
------------

Antes de nos aventurar pelas próximas seções, primeiro vamos gerar um pequeno
programa para GBA. Então:
