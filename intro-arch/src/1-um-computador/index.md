Afinal: O que é um computador?
==============================

"Uma miserável pequeno autômato linearmente limitado" - Autor Desconhecido

Se estivermos falando de um computador físico como o que você está utilizando
agora mesmo, essa seria a definição que um cientista da computação de ponta
daria. Mas como seres humanos (e não cientistas da computação), podemos
visualizar um computador da seguinte forma:

```mermaid
graph LR
    N[Alguém pro qual você entrega<br> instruções em uma caixinha e<br> ele executa elas em sequência]

    N
```

Porém essa definição ainda não é tão útil assim, então vamos acompanhar nos
próximos tópicos o que é esse "alguém".
