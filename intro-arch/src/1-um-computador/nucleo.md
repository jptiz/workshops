Núcleo
======


O Núcleo, no fundo no fundo, é um emaranhado de componentes que incluem:
- Alguém que sabe fazer **aritmética** básica
  (somar/subtrair/dividir/multiplicar) e **operações lógicas** ("e"/"ou");
- Alguém que entende o que é a instrução a ser executada no momento e repassa o
  que os outros componentes devem fazer;
- Lugares para guardar os valores que são utilizados nas intruções.

Abrindo então nosso diagrama de "o que é um computador", podemos representá-lo
como:


```mermaid
graph LR
    MI[Instruções]

    DI[Decodificador]
    ULA[Unidade Lógica e Aritmética]

    subgraph Memória
        MI
    end

    subgraph Processador
        subgraph Núcleo
            DI-->ULA
        end
    end

    MI-->DI
```


```mermaid
graph LR
    MI[Instruções]
    MD[Dados]

    IP[Instruction Pointer]
    RPG[Propósito Geral]
    OutrosReg[Outros]

    DI[Decodificador]
    ULA[Unidade Lógica e Aritmética]

    subgraph Memória
        MI
        MD
    end

    IP-->MI
    ULA---MD

    subgraph Processador
        subgraph Núcleo
            DI-->ULA

            ULA---RPG
            subgraph Registradores
                RPG
                IP
                OutrosReg
            end
        end
    end

    MI-->DI
```
