#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# Options
# ------------------------------------------------------------------------------
DEFAULT_BOOKS="materials git sh latex how-web-works pytest intro-arch unit-tests"
OUTPUT="public"

# ------------------------------------------------------------------------------
# The program
# ------------------------------------------------------------------------------
DEFAULT="\033[0m"

GREEN="\033[90m"
RED="\033[91m"
YELLOW="\033[93m"
OTHER="\033[92m"

INFO="[${YELLOW}INFO${DEFAULT}]"
ERROR="[${RED}ERROR${DEFAULT}]"
GOOD="[${GREEN}GOOD${DEFAULT}]"
SKIP="[${OTHER}SKIP${DEFAULT}]"

BOOKS=""

shopt -s extglob

SUBCOMMANDS="build|clean|serve"

log-good() {
    echo $(printf "${GOOD} $1")
}

log-error() {
    echo $(printf "${ERROR} $1")
}

log-skip() {
    echo $(printf "${SKIP} $1")
}

log() {
    echo $(printf "${INFO} $1")
}

# ------------------------------------------------------------------------------
# Argument-dependent variables
find-mdbook() {
    export PATH="${CARGO_HOME}/bin:${PATH}"
    log "CARGO_HOME: ${CARGO_HOME}/bin"
    log "PATH: ${PATH}"
    log "Finding mdbook in PATH..."
    MDBOOK=$(command -v mdbook)
    if [ ${MDBOOK} ]
    then
        log-good "mdbook found at \"${MDBOOK}\"."
    else
        log "mdbook not found. Trying to fix by using hardcoded path..."
        MDBOOK="${CARGO_HOME}/bin/mdbook"
        X=$(command -v ${MDBOOK})
        echo "X: ${X}"
        if [[ "${X}" ]]
        then
            log-good "Ok, works."
        else
            log-error "Bad, doesn't work."
        fi
        ${MDBOOK} --help
        exit 1
    fi

    log "Finding mdbook-mermaid in PATH..."

    local mermaid=$(command -v mdbook-mermaid)
    if [[ -n ${mermaid} ]]
    then
        log-good "mdbook-mermaid found at \"${mermaid}\"."
    else
        log-error "mdbook-mermaid not found."
        log "Finding mdbook in cargo list..."
        mermaid=$(cargo install --list | grep "^mdbook-mermaid ")
        if [[ -n "${mermaid}" ]]
        then
            log "Ok, mdbook-mermaid found in cargo list: \"${mermaid}\"."
        else
            log-error "Bad, mdbook-mermaid not found at all."
        fi
        exit 1
    fi
}

check-modes() {
    local args=${@:2}
    for arg in ${args}
    do
        echo "parsing arg ${arg}"
        case $arg in
            -q|--quiet)
                QUIET=1
                log "! ! ! Q U I E T ! ! !"
                ;;
            -l|--local)
                LOCAL=1
                log "Building locally."
                ;;
            *)
                echo "    Queuing book \"${arg}\"."
                BOOKS="${BOOKS} ${arg}"
                ;;
        esac
    done
}

# ------------------------------------------------------------------------------
# Functions
build-book() {
    local book=$1

    echo "[INFO] Building ${book}..."
    "${MDBOOK}" build "${book}"
    echo "------------------------ Done building ${book} ------------------------"
}

fix-js-css-path() {
    if [ "$1" = "--css" ]
    then
        local ext="css"
        local tag="href"
        shift
    else
        local ext="js"
        local tag="src"
    fi

    local pat="${tag}=\"\(.*\)\(special-.*.${ext}\)\""

    if [[ ${LOCAL} -eq 1 ]]
    then
        BASEPATH=""
        local dst="${tag}=\"${BASEPATH//\//\\/}\/${ext}\/\2\""
        echo "Fixing ${ext} paths (local build)..."
        echo "    pat -> ${pat}"
        echo "    dst -> ${dst}"
    else
        local dst="${tag}=\"\/workshops\/${ext}\/\2\""
        echo "Fixing ${ext} paths..."
        echo "    -> pat: ${pat}"
        echo "    -> dst: ${dst}"
    fi

    for file in $(find -name "*.html")
    do
        sed -i "s/${pat}/${dst}/g" "${file}"
    done

    for subdir in css js
    do
        mkdir -p "${OUTPUT}/${subdir}"
        cp "${subdir}"/* "${OUTPUT}/${subdir}"
    done

	for book in ${BOOKS}
	do
		cp $(readlink -f "${OUTPUT}/"*.js) "${OUTPUT}/${book}/"
        [[ -z ${QUIET} ]] && ls -l "${OUTPUT}/${book}"
	done

	log "Fixed ${ext} paths in $(readlink -f "${OUTPUT}/")."
	[[ -z ${QUIET} ]] && ls -l "${OUTPUT}/"
}

nofun() {
    if [ -z $1 ]; then
        echo "Usage: ${0} <function>"
    else
        echo "No function \"${1}\"."
    fi

    echo "Available functions: "
    echo "    $(echo ${SUBCOMMANDS} | sed 's/|/\n    /g')"
    exit 1
}

# Subcommands

build() {
    echo "Build flags: "
    echo "    QUIET: ${QUIET}"
    echo "    LOCAL: ${LOCAL}"

    books=${BOOKS}

    if [[ -z ${books} ]]
    then
        books=${DEFAULT_BOOKS}
        echo "Building default books: ${books}"
    else
        echo "Building specific books: ${books}"
    fi

    for book in ${books}
    do
        build-book "${book}"
    done

    fix-js-css-path $1
    fix-js-css-path --css $1

    cp -r materials/src/res "${OUTPUT}/"
}

clean() {
    rm -rf book
}

run-subcommand() {
    local subcommand="@(${SUBCOMMANDS})"
    case $1 in
        ${subcommand})
            $*
            ;;
        *)
            nofun $*
            ;;
    esac
}

find-mdbook
check-modes $*
run-subcommand $*
