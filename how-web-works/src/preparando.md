Preparando seu ambiente de Desenvolvimento
==========================================

Antes de começarmos a implementação de HTTP em si, é importante deixar
preparado o ambiente que será utilizado para desenvolver.

O que será utilizado neste projeto:
- Python 3
- Um editor de código qualquer
