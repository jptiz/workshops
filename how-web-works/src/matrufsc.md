Exemplo de arquitetura: MatrUFSC
================================

Para ver de uma forma um pouco mais próxima sobre a Web, vejamos como é
organizado internamente o [MatrUFSC](pet.inf.ufsc.br/matrufsc).

O Servidor
----------

O servidor do MatrUFSC roda um processo do [nginx](https://www.nginx.com/)
(lê-se: "engine-X", com o X como em "conexão"). Ele controla como são tratadas
todas as requisições HTTP/HTTPS, ouvindo então eventos nas portas 80 e 443. Ao
longo das aplicações web, é comum tanto a porta 80 quanto a porta 8080 serem
utilizadas para requisições HTTP (mas isso [não as faz serem a mesma
porta](https://www.quora.com/Are-port-80-and-8080-the-same/answer/Gopal-Pun-Magar-1)).

```mermaid
graph LR
    A[Backend Flask]
    U[UWSGI]
    N[nginx]
    I[Internet]

    A---U

    U-- 127.0.0.1:5000 ---N

    N-- 0.0.0.0:80 ---I

    style I fill:#ffffff, stroke:#ffffff;
```

### Nginx recebe todas as requisições HTTP

O Nginx é responsável por atender as requisições HTTP de qualquer origem,
incluindo requisições externas (por isso "0.0.0.0:80": 0.0.0.0, nesse contexto,
significa "qualquer IPv4" e 80 é a porta padrão HTTP).

No servidor do MatrUFSC, o Nginx está configurado da seguinte forma:
- Se a requisição for para `/matrufsc` ou `/matrufsc/`, ele vai fornecer o **HTML
  estático** da página do MatrUFSC.;
- Se a requisição for para `/matrufsc/<alguma-coisa>`, ele vai apenas
  redirecionar as requisições para a porta em que está rodando o processo
  uWSGI.

O **estático** quer dizer que a página do MatrUFSC não é gerada dinamicamente
pelo servidor. Por exemplo, no Facebook a página totalmente depende de
informações do servidor (por exemplo, para carregar qual o seu nome, foto de
perfil, etc.), gerando um HTML durante a execução. No caso do MatrUFSC, o HTML
é sempre o mesmo, sendo apenas fornecido direto por um arquivo HTML. A parte do
HTML estático do MatrUFSC é o módulo chamado
[capim](https://github.com/pet-comp-ufsc/capim).

### uWSGI serve a API Python do MatrUFSC

O MatrUFSC fornece uma [API para acesso aos
horários](https://github.com/pet-comp-ufsc/moita/blob/master/app/moita.py).
Então é possível ver os horários de algum identificador salvo acessando
`pet.inf.ufsc.br/matrufsc/load/<identificador>`, por exemplo: se foi salvo um
horário com o identificador "AlunoCCO", ele pode ser acessado com
[http://pet.inf.ufsc.br/matrufsc/load/AlunoCCO](http://pet.inf.ufsc.br/matrufsc/load/AlunoCCO).

Os acessos a essa API são gerenciados pelo uWSGI, que roda na porta 5000 do
servidor do MatrUFSC. "127.0.0.1", nesse caso, é um IP reservado que representa
**a própria máquina** (ou seja, o uWSGI do servidor do MatrUFSC não tem acesso
ao meio externo, apenas a redirecionamentos da própria máquina). Assim, o Nginx
é quem tem acesso externo e apenas os redireciona localmente ao uWSGI, que nem
irá saber se a requisição foi originalmente da própria máquina ou de outra.

Assim, é possível fazer APIs Python utilizando uWSGI. No caso do MatrUFSC, para
isso existe um arquivo INI para o
[moita](https://github.com/pet-comp-ufsc/moita) (módulo Backend do MatrUFSC)
que contém:

```ini
[uwsgi]
base = <pasta-do-código-do-moita>
plugin = python
...
virtualenv = %(base)/.venv
wsgi-file = app/moita.py
chown-socket = http
logto = <arquivo-de-logs-do-moita>
```

### Backend processa as requisições

O Backend do MatrUFSC foi feito utilizando a biblioteca Flask. No código, para
cada função que se queira que faça parte da API Web, é especificado quais URLs
a invocam para qual método (GET, POST, etc.), por exemplo, a API via
`/matrufsc/load/<id>` comentada anteriormente é definida com:

```python
@map.route('/load/<identifier>', methods=['GET'])
def load_timetable(identifier):
	...
```

Essa função, no final, retorna uma tupla `(corpo da resposta, status)` em que o
corpo da resposta é um JSON com os horários daquele identificador, e status é
200, para dizer que a requisição foi executada com sucesso. A Flask irá pegar o
resultado dessa função e devolvê-la (na forma de uma resposta HTTP) ao uWSGI,
que por sua vez irá repassá-la ao Nginx, que por sua vez irá  repassá-la ao
requisitante.

O requisitante, então, irá receber uma resposta HTTP como:

```http
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 22719
Content-Type: text/html; charset=utf-8
Server: nginx/1.14.0
...
    outros headers
...

{"versao":5,"campus":"FLO", [... conteúdo JSON ...]


```

Você pode ver os headers de requisição/resposta nas ferramentas do
desenvolvedor do
[Chrome](https://www.mkyong.com/computer-tips/how-to-view-http-headers-in-google-chrome/)
e do
[Firefox](https://o7planning.org/en/11637/how-to-view-http-headers-in-firefox).
