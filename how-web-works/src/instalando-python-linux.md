Linux
=====

Possivelmente Python já vem instalado na sua distribuição, você pode verificar
com:

```console
$ python --version
```

- Se for Python 3, então está tudo certo.
- Se for Python 2, verifique se Python 3 está instalado:

```console
$ python3 --version
```

- Se não estiver, então instale-o (siga os passos nas seções abaixo).
- Se estiver, então só crie um `alias` de `python` para `python3`. Infelizmente
  isso não é tão padronizado, mas costuma ser apenas editar o arquivo
  `/home/<seu-usuário>/.profile` adicionando:

```bash
export alias python=python3
```

### Instalação

Ubuntu/Debian-based:

```bash
$ sudo apt install python3
```

ArchLinux:

```bash
$ sudo pacman -S python
```
