A Internet
==========

Por mais simples que possa parecer, a Internet é, no fundo, apenas uma
definição de modelo de comunicação.
Esse modelo é definido em 5 camadas:

| Camada        | Protocolos             | Endereçamento     |
| ------------- | ---------------------- | ----------------- |
| 5. Aplicação  | HTTP, SMTP, ...        | -                 |
| 4. Transporte | TCP, UDP               | Protocolo + Porta |
| 3. Rede       | IP                     | Endereço IP       |
| 2. Enlace     | Ethernet, Wi-fi        | Endereço MAC      |
| 1. Física     | 10 Base T, 802.11, ... | -                 |

De maneira resumida:
1. **Camada física**: se preocupa em descrever, além da (de)codificação dos
   bits, detalhes de transmissão e recepção de sinais, como a frequência em que
   eles operam. Por exemplo, o padrão IEEE 802.11, utilizado para Wi-fi,
   descreve que algumas das frequências dos sinais da rede sem fio são 2.4, 3 e
   5GHz.
2. **Camada de enlace**: se preocupa em descrever como os _frames_ são criados
   ou fornecidos à camada física. _Frames_ são sequências padronizadas de bits
   que contêm informações como o endereço físico de quem enviou, do
   destinatário, do roteador, os próprios dados sendo enviados, etc.
3. **Camada de rede**: descreve informações que conterão os pacotes enviados
   pela rede, como endereços IP, protocolo utilizado na comunicação, o tamanho
   e os dados do pacote. Esses pacotes serão então repassados à camada de
   enlace.
4. **Camada de transporte**: estabelece a comunicação entre aplicações
   executando em diferentes _hosts_.
5. **Camada de aplicação**: abriga as aplicações em si e os protocolos de
   comunicação entre elas.
