# Summary

- [A Internet](./a-internet.md)
    - [TCP/UDP](./tcp-udp.md)
    - [A Web](./web.md)
    - [O HTTP](./http.md)
    - [Exemplo de arquitetura: MatrUFSC](./matrufsc.md)

- [Preparando-se](./preparando.md)
    - [Instalando Python](./instalando-python.md)
        - [Linux](./instalando-python-linux.md)
        - [Windows](./instalando-python-windows.md)
    - [Editores de Código](./editores-de-codigo.md)

- [Implementando HTTP](./implementando-http.md)
    - [Desenvolvendo em Python](./desenvolvendo-em-python.md)
    - [Poetry](./poetry.md)
    - [Sockets](./sockets.md)
    - [Enviando uma requisição](./enviando-uma-requisicao.md)
    - [Validando respostas](./respostas.md)
    - [Código completo da 1ª implementação](./codigo-primeira-implementacao.md)

- [Refinando a implementação](./refinando.md)
    - [Suporte a Criptografia](./criptografia.md)
    - [Suporte a Compressão](./compressao.md)
    - [Código final](./codigo-final.md)

[Leituras complementares](./leituras-complementares.md)
