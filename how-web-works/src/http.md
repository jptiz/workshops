HTTP
====

Um dos protocolos de comunicação entre aplicações é o próprio HTTP. A
comunicação entre aplicações com HTTP é dada simplesmente pelo envio e
recebimento de textos, desde que estejam em um formato específico.

### Por que "HTTP"?

Vem de _**H**yper**t**ext **T**ransfer **P**rotocol_. Um hypertexto é
simplesmente texto com referências navegáveis para outros textos (ou seja, em
resumo: texto com possibilidade de se ter links), sendo uma das linguagens de
hypertexto mais comuns a própria HTML (_**H**yper**T**ext **M**arkup
**L**anguage_).

### Um exemplo

Suponha que Ada queira receber o conteúdo da página
`/posts/post-interessante.html`, localizada no servidor do Bob
(`servidordobob.com`). Utilizando HTTP, o acontecerá é:
- Ada envia uma mensagem HTTP no formato de uma **requisição** para o endereço
  do servidor de Bob, tendo como destino a porta 8080;
- Bob receberá a requisição, verá se ela é válida (se a página existe, se há
  permissão de acesso para Ada, etc.), e enviará uma nova mensagem HTTP com uma
  **resposta**;
- Ada receberá a resposta, que indica se tudo deu certo na requisição e, caso
  tenha dado certo, a resposta terá o conteúdo HTML da página requisitada por
  Ada.

O formato de uma requisição HTTP é:

```
<Método> <Caminho> <Protocolo>/<Versão>
<Headers>

<Corpo>


```

Perceba que é obrigatório o uso de duas quebras de linha ao final do texto: são
elas que indicam para a outra aplicação que ali acaba o conteúdo da mensagem.

- O **Método** identifica a intenção da requisição, e alguns dos principais
  valores são:

  | Método    | Intenção                                      |
  | --------- | --------------------------------------------- |
  | `GET`     | Receber uma informação (ex: uma página HTML). |
  | `POST`    | Alterar alguma informação.                    |
  | `PUT`     | Adicionar uma informação nova.                |
  | `DELETE`  | Excluir uma informação.                       |

  Outros valores são `UPDATE`, `CONNECT, `OPTIONS, `TRACE`, `PATCH` e `HEAD`,
  porém eles são extremamente incomuns. Vale lembrar que eles são meramente
  convenção: se um programa de servidor quiser fornecer conteúdo com DELETE ou
  inserir uma informação com GET, **ele pode**.

- O **Caminho** é qual o conteúdo que se quer acessar naquele servidor.
- O **Protocolo** (e sua **versão**) são justamente o que seus nomes
  simbolizam, servindo apenas para garantir "Ei, esta mensagem está no formato
  da versão 1.1 do HTTP."

### O lado de Ada

O exemplo da requisição HTTP de Ada pode ser:

```
GET /posts/post-interessante.html HTTP/1.1
Content-Type: text/html; charset=utf-8
Content-Length: 13

Hello, World!


```

### O lado do servidor de Bob

Quando o servidor do blog do Bob receber a requisição (e fazer as devidas
validações), supondo que o conteúdo da página "/posts/post-interessante.html"
seja:

```html
<!doctype html>
<body>
    <article>
        <h1>Um post interessante</h1>

        Cheio de conteúdo interessante.
    </article>
</body>
```

O que o servidor fará é enviar uma mensagem de **resposta** com:
- O **protocolo/versão** da mensagem (para confirmar que a resposta está no
  mesmo protocolo da requisição);
- O **status** de retorno, que é um número que simboliza se a requisição foi
  aceita com sucesso e tudo deu certo (status 200), ou se aconteceu algum erro
  no servidor enquanto processava a requisição (status 500), ou ainda se o
  caminho que se tentou acessar não existe (status 404).
- Uma descrição do status de retorno (meramente acompanhando o status, por
  exemplo: "200 OK", "500 Internal Server Error", etc.).

Sendo assim, a resposta HTTP pode ser:

```
HTTP/1.1 200 OK
Content-Type: texto/html; charset=utf-8
Content-Length: 138

<!doctype html>
<body>
    <article>
        <h1>Um post interessante</h1>

        Cheio de conteúdo interessante.
    </article>
</body>
```
