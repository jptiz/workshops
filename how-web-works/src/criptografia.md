Suporte a Criptografia
======================

Implementar suporte a TLS/SSL com _sockets_ em Python é extremamente simples: o
pacote `ssl` da biblioteca padrão provê uma função `wrap_context(socket,
hostname)` que nos devolve um novo _socket_, porém com TLS incluído. O
`wrap_context` em si é um método de um `ssl.SSLContext`, que auxilia no
gerenciamento de configurações e certificados, e pode ser gerado a partir da
função `ssl.create_default_context()`.

Primeiramente, precisamos importar `ssl`:

```python
# (Adicione aos `imports`. Favor manter ordem alfabética)
import ssl
```


Então, tudo que precisamos é substituir
trechos do nosso `main` incluindo um `SSLContext` e aplicando ele ao nosso
_socket_:

```python
def main(args: List[str]):
    context = ssl.create_default_context()

    hostname = args[1]

    with socket.create_connection((hostname, 443)) as sock:
        with context.wrap_socket(sock, server_hostname=hostname) as s:
            test_request(s)
```

E pronto, temos tudo que precisamos para ter suporte a TLS/SSL.
