Recomendações de editores
=========================

Alguns editores de código independentes de linguagem que recomendamos são:
- [Visual Studio Code](vscode.com): Tem se mostrado um editor muito competente.
  Plugins são fáceis de instalar, o visual é simples e satisfatório, os
  recursos disponíveis são bastante flexíveis, configuráveis, fáceis de usar,
  intuitivos, etc., e é extremamente fácil de conseguir suporte à sua linguagem
  favorita. Fortemente recomendado.
- [Sublime Text](sublimetext.com): É mais rápido do que o VSCode no geral, mas
  tem menos suporte nativo, alguns recursos não são tão robustos, mas ainda
  assim é um editor muito potente, com diversos plugins fáceis de
  instalar/usar, configurações tão flexíveis quanto às do VSCode, dentre outras
  vantagens. Infelizmente, a cada 10 vezes que um arquivo é salvo, uma mensagem
  aparece perguntando se você não quer doar um dinheiro à equipe do Sublime
  (isso deixa de acontecer depois que você faz a doação).
- [Atom](atom.io): É outro editor bastante potente, mas não tão leve quanto o
  Sublime Text ou o VSCode. Ao menos possui um visual simples e atrativo,
  plugins, então caso não consiga se acostumar com os outros dois, ainda há o
  Atom.

