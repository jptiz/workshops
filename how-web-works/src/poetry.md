Poetry
======

Instalando Poetry
-----------------

Estando em um terminal (ou `cmd` no Windows), é possível instalar Python
através do PIP (gerenciador de pacotes de Python que **já vem com a
instalação**) com o comando:

```console
$ pip install --user poetry
```

A flag `--user` serve para que a instalação de Poetry seja apenas para o
usuário atual (e portanto não precise de permissão de administrador).

Criando um projeto
------------------

Poetry permite criar um projeto Python novo com:

```console
$ poetry new <nome do projeto>
```

Sendo assim, criaremos um projeto chamado `simplehttp`:

```console
$ poetry new simplehttp
Created package simplehttp in simplehttp
```

O projeto criado terá a seguinte estrutura:

```tree
simplehttp
├── pyproject.toml
├── README.rst
├── simplehttp
│   └── __init__.py
└── tests
    ├── __init__.py
    └── test_simplehttp.py
```

### O que precisamos

Faremos uma biblioteca simples:
- Sem testes unitários (pedimos as mais profundas desculpas :( do fundo do
  coração);
- Com apenas um único módulo (ou seja, apenas um único .py).

### O que não precisamos

Como não precisamos de mais de um módulo, não será necessário ter uma pasta
estruturada com um `__init__.py` dentro. Isso serve apenas para definir pacotes
compostos de mais de um módulo, o que não é nosso caso.

Então, não precisaremos:
- A pasta **interna** `simplehttp` (a que tem o `__init__.py`);
- A pasta `tests`.

### O que fazer então

Transformaremos a pasta interna simplehttp em um arquivo `simplehttp.py` e
removeremos a pasta `tests`. No terminal, podemos fazer isso com:

```shell
$ cd simplehttp                      # Troca para a pasta EXTERNA `simplehttp`

$ # Mostra o conteúdo da pasta (para garantir que estamos na pasta correta):
$ [simplehttp] ls
pyproject.toml  README.rst  simplehttp/  tests/

$ [simplehttp] rm -r simplehttp      # Exclui a pasta `simplehttp`
$ [simplehttp] touch simplehttp.py   # Cria um arquivo em branco

$ [simplehttp] rm -r tests           # Exclui a pasta `tests`
```

Com isso feito, a estrutura da pasta do projeto deve ser:

```fs
simplehttp
├── pyproject.toml
├── README.rst
└── simplehttp.py
```

Último passo: instalar o próprio pacote
---------------------------------------

Por fim, podemos pedir ao Poetry para instalar nosso próprio pacote em uma
Virtualenv separada para nosso projeto, assim trabalhamos com ele de um jeito
bem parecido como os usuários de nossa biblioteca utilizariam:

```console
$ poetry install
Creating virtualenv simplehttp-py3.7 in /home/user/.cache/pypoetry/virtualenvs
Updating dependencies
Resolving dependencies... (2.7s)

Writing lock file


Package operations: 9 installs, 0 updates, 0 removals

  - Installing zipp (0.5.1)
  - Installing importlib-metadata (0.18)
  - Installing atomicwrites (1.3.0)
  - Installing attrs (19.1.0)
  - Installing more-itertools (7.1.0)
  - Installing pluggy (0.12.0)
  - Installing py (1.8.0)
  - Installing six (1.12.0)
  - Installing pytest (3.10.1)
  - Installing simplehttp (0.1.0)
```
