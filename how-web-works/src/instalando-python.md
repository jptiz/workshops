Instalando Python
=================

Qual versão instalar?
---------------------

Primeiro de tudo: **não use Python 2**. [O suporte de Python 2 está com os dias
contados](https://pythonclock.org/).

Tendo Python 3, a versão recomendada é sempre **a mais atual** (Python 3.7
atualmente, e Python 3.8 sai em Outubro/2019), mas para estabelecer uma versão
mínima, sugerimos **pelo menos a 3.6**.
