Windows
=======

Primeiramente, verifique se Python já está instalado (você pode verificar no
[painel de reparar/remover
programas](https://support.microsoft.com/pt-br/help/4028054/windows-10-repair-or-remove-programs)).

### Se já tiver Python instalado

Certifique-se de que você pode executar `python` pelo CMD:
1. Tecle <kbd>Windows</kbd>+<kbd>R</kbd>;
2. Digite `cmd` e dê um <kbd>Enter</kbd>;
3. Na tela do CMD, execute:

   ```console
   python --version
   ```

  Se aparecer a versão de Python que está instalada, então está tudo certo. Se
  aparecer "'python' não é reconhecido um comando interno ou externo", então
  você precisa [adicionar Python ao seu
  PATH](https://projects.raspberrypi.org/en/projects/using-pip-on-windows/5).

### Se Python não estiver instalado

1. Baixe o instalador de Python direto pelo [site oficial de
   Python](https://www.python.org/downloads/windows/):
    1. Em **"Latest Python 3 Release"**;
    2. Vá na seção **Files** ao fim da página;
    3. Pegue o **"Windows x86-64 executable installer"**.
2. Terminando de baixar o instalador, execute-o e marque a opção "Adicionar
   Python 3.x ao PATH" (imagem abaixo, retirada deste [tutorial de instalação
   de
   Python](https://realpython.com/installing-python/#step-2-run-the-installer));
3. Clique em "Instalar agora", abra um terminal e rode de novo:

    ```console
    python --version
    ```

![Imagem da tela de instalação com "Adicionar Python ao PATH"
marcado](https://files.realpython.com/media/win-install-dialog.40e3ded144b0.png)

