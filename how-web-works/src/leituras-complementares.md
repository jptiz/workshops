Leituras Complementares
=======================

1. [TCP/IP Five-Layer Software Model Overview - Microchip Developer
   Help](https://microchipdeveloper.com/tcpip:tcp-ip-five-layer-model)
2. [What exactly is a MAC Address used for -
   HowToGeek](https://www.howtogeek.com/169540/what-exactly-is-a-mac-address-used-for/)
3. [HTTP Messages - Mozilla Developer
   Network](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)

