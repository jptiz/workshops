"Usage:"
========

#### my-script.sh

```bash
#!/usr/bin/env bash
EXEC_NAME=$(basename $0)
VERSION="v0.0.1"

USAGE="Usage: ${EXEC_NAME} <operation> <filename>
       ${EXEC_NAME} -h
       ${EXEC_NAME} -v
"

if [[ $1 == "create" ]];
then
    echo "Creating ${FILENAME}"
elif [[ $1 == "edit" ]];
then
    echo "Editing ${FILENAME}"
elif [[ $1 == "-h" ]];
then
    echo $USAGE
elif [[ $1 == "-v" ]];
then
    echo "My project ${VERSION}"
else
    echo "Missing operation!"

    echo $USAGE
fi
```
