Lendo argumentos
================

#### my-script.sh

```bash
#!/usr/bin/env bash

echo "Args: $*"

if [[ $1 == "welcome" ]];
then
    echo "Welcome to my shell!"
elif [[ $1 == "bye" ]];
then
    echo "Goodbye, shell..."
else
    echo "Missing argument!"
fi
```
