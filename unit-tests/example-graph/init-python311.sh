#!/usr/bin/bash

export PYENV_ROOT="${HOME}/.pyenv"
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
export PATH="${HOME}/.pyenv/bin:${PATH}"
eval "$(pyenv init -)"
exec "${SHELL}"
pyenv install 3.11
pyenv shell 3.11
