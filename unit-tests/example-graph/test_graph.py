"""Simple unit tests for the graph module"""
import pytest

from graphex import Graph


def test_create_graph_without_values() -> None:
    graph = Graph()

    assert len(graph.vertices) == 0



def test_create_graph_with_values() -> None:
    graph = Graph({"A", "B", "C"})

    assert len(graph.vertices) == 3
    assert graph["A"]
    assert graph["B"]
    assert graph["C"]

    with pytest.raises(KeyError):
        assert graph["D"]


def test_remove_vertex() -> None:
    graph = Graph({"A", "B"})
    graph.remove("A")

    assert graph.vertices.keys() == {"B"}


def test_order_after_creation() -> None:
    assert Graph().order() == 0
    assert Graph({"A", "B"}).order() == 2


def test_order_after_add() -> None:
    graph = Graph()
    graph.add("A")
    assert graph.order() == 1

    graph.add("A")
    assert graph.order() == 1

    graph.add("B")
    assert graph.order() == 2


def test_order_after_remove() -> None:
    graph = Graph({"A", "B"})
    graph.remove("A")
    assert graph.order() == 1
    graph.remove("B")
    assert graph.order() == 0


def test_link_neighbours_one_direction() -> None:
    """Tests if a graph is able to link and unlink vertices as neighbours."""
    graph = Graph[str]({"A", "B", "C"})

    graph.link("A", "B")

    assert graph["A"].neighbours == {"B"}
    assert graph["B"].neighbours == {"A"}

    graph.link("A", "C")

    assert "B" in graph["A"].neighbours
    assert "A" not in graph["A"].neighbours
    assert "C" in graph["A"].neighbours


def test_cyclic_neighbours() -> None:
    graph = Graph[str]({"A", "B", "C"})
    graph.link("A", "A")

    assert "A" in graph["A"].neighbours

    graph.link("A", "B")

    assert "A" in graph["A"].neighbours
    assert "B" in graph["A"].neighbours


def test_link_neighbours_then_unlink() -> None:
    """Tests if a graph is able to link and unlink vertices as neighbours."""
    graph = Graph[str]({"A", "B", "C"})

    graph.link("A", "B")
    graph.unlink("B", "A")

    assert graph["A"].neighbours == set()
    assert graph["B"].neighbours == set()


def test_regular() -> None:
    """Tests if a graph is able to link and unlink vertices as neighbours."""
    graph = Graph[int]()

    graph.add(0)
    graph.add(1)
    graph.add(2)

    graph.link(0, 1)
    graph.link(0, 2)

    assert not graph.regular()

    graph.link(1, 2)

    assert graph.regular()


def test_complete() -> None:
    """Tests if a graph is able to recognize when it's complete or not."""
    graph = Graph[int]()

    graph.add(0)
    graph.add(1)
    graph.add(2)
    graph.add(3)

    assert not graph.complete()

    graph.link(0, 1)

    assert not graph.complete()

    graph.link(0, 2)
    graph.link(0, 3)

    graph.link(1, 2)

    graph.link(2, 3)

    assert not graph.complete()

    graph.link(1, 3)

    assert graph.complete()


def test_transitive_closure() -> None:
    """Tests if a graph is able to generate transitive closures correctly."""
    graph = Graph[int]()

    graph.add(0)
    graph.add(1)
    graph.add(2)
    graph.add(3)
    graph.add(4)
    graph.add(5)

    assert graph.transitive_closure(0) == {0}

    graph.link(0, 1)

    assert graph.transitive_closure(0) == {0, 1}

    graph.link(2, 3)

    assert graph.transitive_closure(2) == {2, 3}

    graph.link(1, 2)

    assert graph.transitive_closure(2) == {0, 1, 2, 3}
    assert graph.transitive_closure(5) == {5}

    graph.link(4, 4)

    assert graph.transitive_closure(4) == {4}

    graph.link(4, 5)

    assert graph.transitive_closure(4) == {4, 5}


def test_connected() -> None:
    """Tests if a graph is able to recognize when it's complete or not."""
    graph = Graph[int]()

    graph.add(0)
    graph.add(1)
    graph.add(2)
    graph.add(3)
    graph.add(4)
    graph.add(5)

    assert not graph.connected()

    graph.link(0, 1)

    assert not graph.connected()

    graph.link(2, 3)
    graph.link(1, 2)
    graph.link(4, 4)
    graph.link(4, 5)

    assert not graph.connected()

    graph.link(0, 4)

    assert graph.connected()


def test_tree() -> None:
    """Tests if a graph is able to recognize when it's a tree or not."""
    graph = Graph[int]()

    graph.add(0)
    graph.add(1)
    graph.add(2)
    graph.add(3)
    graph.add(4)
    graph.add(5)

    assert not graph.is_tree()

    graph.link(0, 1)
    graph.link(0, 2)
    graph.link(3, 4)
    graph.link(4, 5)

    assert not graph.is_tree()

    graph.link(0, 5)

    assert graph.is_tree()

    graph.link(2, 3)

    assert not graph.is_tree()

    graph.unlink(2, 3)

    assert graph.is_tree()

    graph.link(1, 2)

    assert not graph.is_tree()


def test_random_vertex(mocker) -> None:
    order = iter([1, 2, 0])
    mocker.patch("random.choice", lambda values: values[next(order)])

    graph = Graph({1, 2, 3})

    assert graph.random_vertex() == graph[2]
    assert graph.random_vertex() == graph[3]
    assert graph.random_vertex() == graph[1]
