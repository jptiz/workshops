# Summary

[Por que escrever testes unitários?](./Por-que-escrever-testes-unitarios.md)
[Projeto (sem testes) de exemplo](./Projeto-de-exemplo.md)

# Criando uma infraestrutura de testes

- [Conhecendo um framework: Pytest](./Conhecendo-um-framework-pytest.md)
- [Garantindo Cobertura](./Garantindo-Cobertura.md)
- [Fixtures e Mocks](./Fixtures-e-Mocks.md)

# Além dos testes unitários

- [Checagens adicionais](./Checagens-adicionais.md)
- [Git hooks](./Git-hooks.md)
- [Integração Contínua](./Integração-Contínua.md)
