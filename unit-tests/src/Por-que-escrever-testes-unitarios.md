Por que escrever testes unitários?
==================================

O objetivo principal de testes unitários é ter alguma garantia de que, pelo
menos para os cenários conhecidos, o código dá determinados resultados
esperados, quebra com o erro que deveria, cria os arquivos que deveria com o
conteúdo que necessita, e por aí vai.

Ajuda a descobrir erros novos ou antigos
----------------------------------------

Erros escondidos ao longo do código concedem péssimas experiências quando
decidem acontecer em momentos cruciais ou quando se vai corrigi-los porém foram
socados por mudanças em cima de mudanças. Descobrir antecipadamente esses erros.

Ajuda a avaliar a sua API
-------------------------

Vocề escreve seu próprio código, e assim você pode ter uma ideia sobre se é tão
simples utilizá-lo quanto você pensa. Se para escrever um teste simples é
necessário muitas operações, isso pode ser um indicativo de que é necessário
simplificar o código para tarefas simples.

Ajuda a mapear tarefas do projeto
---------------------------------

Às vezes o caminho para começar um projeto, implementar uma nova funcionalidade
ou corrigir um caso específico de uma já existente pode não estar tão claro.
Nesses casos, começar a mapear as tarefas a serem feitas com testes pensando em
pequenas tarefas que precisam acontecer pode ser uma boa estratégia.

Quem for dar manutenção no código depois irá agradecer
------------------------------------------------------

Uma situação terrível e desesperançosa é a de chegar em um projeto já
existente, encontrar um erro e não fazer ideia de em que situações ele se
origina. Como os testes unitários costumam ser pensados em possíveis situações
(sequências de chamadas com diferentes valores para os argumentos de funções,
etc.), é possível utilizar - por exemplo - análises de cobertura para averiguar
quais situações não são testadas e, com isso, encontrar cenários que originem a
falha em questão. Assim, quem for dar manutenção no código - e esse alguém pode
ser você - irá agradecer por ver que os mais diferentes cenários já estavam
sendo testados.
