Garantindo Cobertura
====================

1. Instalar plugin: `poetry add --group dev pytest-cov`
2. Executar Pytest com: `pytest --cov=<pacote ou caminho>`

   ```
   ---------- coverage: platform linux, python 3.10.8-final-0 -----------
   Name            Stmts   Miss  Cover
   -----------------------------------
   graphex.py         79      5    94%
   test_graph.py     118      4    97%
   -----------------------------------
   TOTAL             197      9    95%
   ```

2. Reportar linhas não-cobertas: `pytest --cov=<pasta> --cov-report=term-missing`

   ```console
   ---------- coverage: platform linux, python 3.10.8-final-0 -----------
   Name            Stmts   Miss  Cover   Missing
   ---------------------------------------------
   graphex.py         79      5    94%   53, 80, 93, 102, 156
   test_graph.py     118      4    97%   55-60
   ---------------------------------------------
   TOTAL             197      9    95%
   ```

3. No `pyproject.toml`, habilitar Cobertura em todos os testes:

   ```toml
   [tool.coverage.run]
   source = ["graphex"]

   [tool.pytest.ini_options]
   addopts = "--cov --cov-report=term-missing"
   ```
