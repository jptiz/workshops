Projeto de exemplo
==================

Biblioteca de Grafos
--------------------

Grafos são formas de organizar relações em vértices e arestas. Usaremos como
projeto de exemplo para adicionar testes uma biblioteca de manipulação de
grafos.

As operações mais básicas são:

- Criação do grafo:

  ```python
  graph = Graph() # grafo vazio
  graph = Graph({1, 2, 3}) # grafo com alguns vértices
  ```

- Adicionar/Remover vértices:

  ```python
  graph.add(1)    # vértices: {1}
  graph.add(2)    # vértices: {1, 2}
  graph.remove(1) # vértices: {2}
  ```

- Adicionar/Remover arestas (bidirecionais):

  ```python
  graph = Graph({1, 2, 3})  # graph: (1)  (2)  (3)

  # Cria uma aresta ligando os vértices 1 e 2
  graph.link(1, 2)  # graph: (1)--(2)  (3)

  # Cria uma aresta ligando os vértices 2 e 3
  graph.link(2, 3)  # graph: (1)--(2)--(3)

  # Remove a aresta que liga os vértices 1 e 2
  graph.unlink(1, 2)  # graph: (1)  (2)--(3)
  ```

O código completo da biblioteca:

```python
"""A graph library."""
import random
from dataclasses import dataclass, field
from typing import Iterable, Generic, TypeVar


Value = TypeVar("Value")


def first_of(container: Iterable[Value]) -> Value:
    """Gets the first element of an iterable."""
    return next(iter(container))


@dataclass
class Vertex(Generic[Value]):
    """A graph's vertex."""

    value: Value
    neighbours: set[Value] = field(default_factory=set)

    def degree(self) -> int:
        """Returns the vertex's degree."""
        return len(self.neighbours)


@dataclass
class Graph(Generic[Value]):
    """The graph itself."""

    vertices: dict[Value, Vertex[Value]] = field(default_factory=dict)

    def __init__(self, values: Iterable[Value] | None = None) -> None:
        values = values if values is not None else {}
        self.vertices = {value: Vertex(value) for value in values}

    # Basic operations

    def add(self, v: Value) -> None:
        """
        Adds a vertex to the graph.

        Args:
            v: Vertex to be add.
        """
        self.vertices[v] = Vertex(v)

    def remove(self, v: Value) -> None:
        """Removes a vertex from the graph.

        Args:
            v: Vertex to be removed.
        """
        del self.vertices[v]

    def link(self, v1: Value, v2: Value) -> None:
        """
        Links two vertices by a bidirectional edge.

        Args:
            v1: First vertex.
            v2: Second vertex.
        """
        self.vertices[v1].neighbours.add(v2)
        self.vertices[v2].neighbours.add(v1)

    def unlink(self, v1: Value, v2: Value) -> None:
        """
        Unlinks two vertices.

        Args:
            v1: First vertex.
            v2: Second vertex.
        """
        self.vertices[v1].neighbours.discard(v2)
        self.vertices[v2].neighbours.discard(v1)

    def order(self) -> int:
        """
        Graph's order."""
        return len(self.vertices)

    def random_vertex(self) -> Vertex[Value]:
        """Gets a random vertex from the graph."""
        return random.choice(list(self.vertices.values()))

    def neighbours(self, key: Value) -> set[Value]:
        """
        Gets a set with a vertex's neighbours.

        Args:
            key: The vertex.
        """
        return self.vertices[key].neighbours

    def degree(self, key: Value) -> int:
        """
        Gets a vertex's degree.

        Args:
            key: The vertex.
        """
        return self.vertices[key].degree()

    # Derived actions

    def regular(self) -> bool:
        """Checks if the graph is regular."""
        common_degree = len(self.random_vertex().neighbours)

        for v in self.vertices.values():
            if v.degree() != common_degree:
                return False

        return True

    def complete(self) -> bool:
        """Checks if the graph is complete."""
        for src in self.vertices:
            for other in self.vertices:
                if src != other and other not in self[src].neighbours:
                    return False

        return True

    def transitive_closure(self, src: Value) -> set[Value]:
        """
        Gets the transitive closure starting from the given source vertex.

        Args:
            src: Reference (source) vertex.
        """

        def search_transitive_closure(v: Value, visited: set[Value]) -> set[Value]:
            """
            Searchs for a transitive closure.

            Args:
                v: Reference vertex.
                visited: Already visited vertices.
            """
            visited.add(v)
            for v_ in self[v].neighbours:
                if not v_ in visited:
                    search_transitive_closure(v_, visited)

            return visited

        return search_transitive_closure(src, set())

    def connected(self) -> bool:
        """
        Checks if the graph is connected. An empty graph is considered
        disconnected.
        """
        if not self.vertices:
            return False

        trans = self.transitive_closure(first_of(self.vertices))
        values = {v.value for v in self.vertices.values()}
        return len(trans ^ values) == 0

    def is_tree(self) -> bool:
        """Checks if the graph is a tree."""
        return self.connected() and not self.has_cycle()

    def has_cycle(self, src: Value | None = None) -> bool:
        """
        Check if the graph has a cycle.

        Args:
            v: Vertex to be checked against visited set.
                        If None, begins searching recursively for a cycle.
            prev: Reference vertex.
            visited: Set of already visited vertices.
        """

        def _has_cycle(
            v: Value,
            prev: Value,
            visited: set[Value],
        ) -> bool:
            visited = set() if visited is None else visited

            if v in visited:
                return True

            visited.add(v)
            for v_ in self[v].neighbours:
                if v_ != prev and _has_cycle(v_, v, visited):
                    return True
            visited.remove(v)

            return False

        src = src if src is not None else first_of(self.vertices)
        return _has_cycle(src, src, set())

    # Extra

    def __getitem__(self, key: Value) -> Vertex[Value]:
        """
        Overrides operator [] to get a vertex by a given key.

        Args:
            key: The vertex's key.
        """
        return self.vertices[key]
```

Aplicação Web (libru)
---------------------

Para ensinar ligeiramente o conceito de Fixtures, utilizaremos a LibRU, uma
biblioteca que fornece o cardápio do dia do RU.

Link para o código-fonte: https://github.com/caravelahc/libru.
