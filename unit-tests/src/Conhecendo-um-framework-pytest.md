Conhecendo um framework: Pytest
===============================

É extremamente importante em testes unitários (e outras ferramentas, na
verdade) partir de algo que já está pronto, testado e capaz de fornecer os
recursos mais diversos de testagem. Para Python, não há motivos para não
utilizar **Pytest**.

Primeiramente, se a questão é tornar o projeto mais robustos com testes, então
é adequado também trazer essa robustez em tudo começando por utilizar `poetry`
para gerenciar as dependências do projeto e de desenvolvimento:

```console
$ poetry init
  # [... preencher informações ...]
$ poetry add --group dev pytest  # Se já não foi feito durante o `poetry init`
$ poetry install                 # Instala as dependências em uma virtualenv
$ poetry shell                   # Reinicia o Shell com a virtualenv ativada
```

Criando testes
--------------

O Pytest reconhece testes em arquivos com o nome iniciado em "test\_" ou
terminado em "\_test". Cada teste é uma função com essa mesma regra
("test\_algo" ou "algo\_test"). Por exemplo, criando o arquivo `test_graph.py`:

```python
from graphex import Graph

def test_create_empty_graph() -> None:
    """
    Tests if creating a graph with default constructor really generates an
    empty graph.
    """
    graph = Graph()

    assert graph.vertices == set()
```
