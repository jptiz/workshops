Ramificando um projeto
======================

É bastante comum querer ramificar um projeto, seja separando por features em
desenvolvimento, seja separando por frontes da equipe de desenvolvimento, etc.

Em sistemas de versionamento, essas ramificações se chamam **branches**.
