Aplicando alterações de outra branch
====================================

Supondo o projeto no seguinte estado:

```mermaid
graph TD;
    M0["master (sem commits)"];
    M1["master: commit 1"];
    M2["master: commit 1"];
    M3["master: commit 1"];
    D1["dev: commit 1"];
    D2["dev: commit 2"];

    M0--git commit -m 'commit 1'-->M1
    M1-->M2
    M2-->M3
    M1--git checkout -b dev-->D1
    D1--git commit -m 'commit 3'-->D2
    ;
```

Aplicar as alterações de `dev` na `master` pode ser feito da seguinte forma:

1. Trocar para a branch `master`;
2. Executar:

   ```console
   $ git merge dev
   ```

O Git irá tentará fazer o `merge` automaticamente. Caso haja conflito entre as
últimas alterações do `dev` em relação às do `master`, você terá de resolvê-las
antes de continuar o `merge`. Caso dê tudo certo, o repositório estará em um
formato como:

```mermaid
graph TD;
    M0["master (sem commits)"];
    M1["master: commit 1"];
    M2["master: commit 1"];
    M3["master: commit 1"];
    M4["master: commit 2"];
    D1["dev: commit 1"];
    D2["dev: commit 2"];
    D3["dev: commit 2"];

    M0--git commit -m 'commit 1'-->M1
    M1-->M2
    M2-->M3
    M3-->M4
    M1--git checkout -b dev-->D1
    D1--git commit -m 'commit 2'-->D2
    D2--git merge dev-->M4
    D2-->D3
    ;
```
