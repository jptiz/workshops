Criando uma ramificação
=======================

Criar uma ramificação é bastante simples:

```console
$ git checkout -b <nova branch>
```

Por exemplo, se for criada a _branch_ `dev` para cuidar de recursos não estáveis:

```console
$ git checkout -b dev
```

A partir de agora, os `commit`s que forem feitos irão para a _branch_ `dev`.
Dessa forma, fazendo 4 outros commits, o projeto fica parecendo algo como:

```mermaid
graph TD;
    M0["master (sem commits)"];
    M1["master: commit 1"];
    M2["master: commit 2"];
    M3["master: commit 2"];
    M4["master: commit 2"];
    M5["master: commit 2"];
    M6["master: commit 2"];
    D1["dev: commit 2"];
    D2["dev: commit 3"];
    D3["dev: commit 4"];
    D4["dev: commit 5"];

    M0--git commit -m 'commit 1'-->M1
    M1--git commit -m 'commit 2'-->M2
    M2-->M3
    M3-->M4
    M4-->M5
    M5-->M6
    M2--git checkout -b dev-->D1
    D1--git commit -m 'commit 3'-->D2
    D2--git commit -m 'commit 4'-->D3
    D3--git commit -m 'commit 5'-->D4
    ;
```

Checando o `git log` (utilizando `--graph` para ficar melhor de visualizar as
ramificações e `--oneline` para simplificar as informações):

```console
$ git log --graph --oneline
* 0c9aa0b (HEAD -> dev) Adding 'd'.
* 4b88597 Adding 'c'.
* ea94b38 Adding 'b'.
* 81ea5ae Adding 'a'.
* 74192e7 (master) Add code to sample.py.
* cdc8273 First commit.
```

(Observe também que o "HEAD" aponta para a branch `dev`.)
