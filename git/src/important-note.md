Recado importante
=================

Leia a saída dos comandos `git`
-------------------------------


**Leia** as informações que os comandos do `git` te dão quando algo errado ou
inesperado acontece. Em quase 100% das ocorrências, ele está dizendo **exatamente
o que você deve fazer**.

_(Como dizia um grande professor: "Pelo amor de Deus, leia o manual".)_
