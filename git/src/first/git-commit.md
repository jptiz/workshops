Versionando alterações
======================

Uma versão de um projeto é chamada de um **commit**.

```mermaid
graph TD;
    S["Staging Area"]
    R["Repository"]

    S-- git commit -->R
    ;
```

Todo commit **deve ter um autor**. Para se identificar apenas no
repositório atual:

```console
$ git config user.name "<seu nome completo>"
$ git config user.email "<seu e-mail">
```

Caso esteja em seu computador, você pode se identificar para todos os
repositórios com `--global`:

```console
$ git config --global user.name "<seu nome completo>"
$ git config --global user.email "<seu e-mail">
```

Transformar as alterações da Staging Area em um commit
------------------------------------------------------

```console
$ git commit -m "<Resumo das alterações>"
```

Ver diferenças da Staging Area e o último commit
------------------------------------------------

```console
$ git diff
```
