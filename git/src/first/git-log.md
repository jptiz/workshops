Logs
====

É possível ver o histórico de _commits_ com:

```console
$ git log
```

O resultado será algo como:

```console
$ git log
commit cdc827345951ec9e0eba6f2f2fc917d9eda89505 (HEAD -> master)
Author: Seu Nome <seu@email>
Date:   Data do commit

    First commit.
```

Caso haja mais de um log, eles serão exibidos em ordem decrescente de tempo:

```console
$ git log
commit 74192e77bc55edc8a0e5c98c2acb0c0d723a87fa (HEAD -> master)
Author: Seu Nome <seu@email>
Date:   Data do commit

    Add code to sample.py.

commit cdc827345951ec9e0eba6f2f2fc917d9eda89505
Author: Seu Nome <seu@email>
Date:   Data do commit

    First commit.
```

Argumentos interessantes
------------------------

### Simplificar log: `--oneline`

```console
$ git log --oneline
74192e7 (HEAD -> master) Add code to sample.py.
cdc8273 First commit.
```

### Ver commits e branches como um grafo: `--graph`

Será visto melhor mais adiante.

### Ver commits de todas as branches: `--all`

Será visto melhor mais adiante².
