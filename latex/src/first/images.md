Inserindo imagens
=================

Imagens não são padrões como os demais comandos, mas o pacote `graphics` pode
ajudar com elas. Esse pacote oferece a macro
`\includegraphics[<opções>]{<caminho/da/imagem>}`. OBS: você pode omitir a
extensão da imagem.

Você pode colocar quantas opções quiser, separando elas por vírgula. Algumas
das opções disponíveis são:

#### `width`

Serve para definir a largura da imagem. Pode ser útil para redimensionar a
imagem para a largura da região do texto:

```latex
\includegraphics[width=1\textwidth]{sample}
```

Nesse caso, a imagem será redimensionada para 1x a largura da região reservada
para o texto em que a imagem ficará.

#### `height`

Semelhante a `width`, porém para a altura, por exemplo:

```latex
\includegraphics[height=1\textheight]{sample}
```

#### `keepaspectratio`

Semelhante a `width`, porém para a altura.

Prática
-------

1. Baixe a imagem abaixo e salve na mesma pasta do .tex:
   ![Imagem de exemplo]()
2. Inclua o pacote `graphics`;
3. Inclua a imagem que você salvou no final do documento;
4. Teste para ver como ficará o documento;
5. Redimensione a imagem para caber no documento.

Resultado
---------

```latex
\documentclass{article}

\usepackage{graphics}

\title{Aprendendo LaTeX}
\author{Tizpector}
\date{\today}

\begin{document}
    \maketitle

    Texto de exemplo

    \includegraphics[width=1\textwidth,keepaspectratio]{img/sample}
\end{document}
```
